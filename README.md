# Sound Board

Is this like iCarly? Reminds me of the remote from that show. Fun use of audio tags and Javascript.

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).

Victory sound (c) Square Enix

All sounds (c) their respective owners
