const sounds = ['applause', 'boo', 'gasp', 'tada', 'victory', 'wrong']
const buttondiv = document.querySelector('#buttons')
console.log(buttondiv)

sounds.forEach((sound) => {
  const btn = document.createElement('button')
  btn.classList.add('btn')

  btn.innerText = sound

  btn.addEventListener('click', () => {
    stopSongs()

    document.getElementById(sound).play()
  })

  buttondiv.appendChild(btn)
})

function stopSongs() {
  sounds.forEach((sound) => {
    const song = document.getElementById(sound)

    song.pause()
    song.currentTime = 0
  })
}
